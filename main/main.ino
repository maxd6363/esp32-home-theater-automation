#include <IRsend.h>

#define IR_SEND_AMP_PIN 4 //D4
#define IR_SEND_LED_PIN 2 //D2
#define POWER_CONSUPTION_PIN 13 //D13
#define MV_PER_AMP 185 // use 185 for 5A, 100 for 20A Module and 66 for 30A Module
#define WATT_DELTA 50
#define WATT_AVG_CYCLE 35

#define TV_ON_THRESHOLD 140
#define TV_OFF_THRESHOLD 60


IRsend IrSendAmp(IR_SEND_AMP_PIN);
IRsend IrSendLed(IR_SEND_LED_PIN);

bool isTvOn = false;
float avgWatt = 0;
int avgWattCycle = 0;

void togglePowerAmp() {
  IrSendAmp.sendSony(0x540A, 15);
}

void togglePowerLed() {
  IrSendLed.sendNEC(0xFF02FD, 32);
}

void togglePower(){
  togglePowerLed();
  togglePowerAmp();
  Serial.println("toogle power");
}


void setup() {
	Serial.begin(115200);
	Serial.println("Starting up");
  pinMode(POWER_CONSUPTION_PIN, INPUT);


  IrSendAmp.begin();
  IrSendLed.begin();
}

void loop() {
  float watt = getWatt();

  avgWattCycle = (avgWattCycle + 1) % WATT_AVG_CYCLE;
  avgWatt += watt;

  if(avgWattCycle == 0)
  {
    Serial.println(avgWatt / WATT_AVG_CYCLE);

    bool oldTvState = isTvOn;

    if(avgWatt / WATT_AVG_CYCLE > TV_ON_THRESHOLD)
      isTvOn = true;
    if(avgWatt / WATT_AVG_CYCLE < TV_OFF_THRESHOLD)
      isTvOn = false;

    if(oldTvState != isTvOn)
      togglePower();
    
    avgWatt = 0;
  }
}

float getWatt()
{ 
  int readValue;
  int maxValue = 0;
  int minValue = 4096;
  
   uint32_t start_time = millis();

   while((millis() - start_time) < 50)
   {
       readValue = analogRead(POWER_CONSUPTION_PIN);
       if (readValue > maxValue) 
           maxValue = readValue;
       
       if (readValue < minValue) 
           minValue = readValue;
  }
  return (220*(((((((maxValue - minValue) * 5)/4096.0)/2.0) *0.707) * 1000)/MV_PER_AMP))-WATT_DELTA;
}
